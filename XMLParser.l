%{
#include <stdio.h>
#include <stdlib.h>

char* stringRemoveNonAlphaNum(char *str)
{
    unsigned long i = 0;
    unsigned long j = 0;
    char c;
 
    while ((c = str[i++]) != '\0')
    {
        if (isalnum(c))
        {
            str[j++] = c;
        }
    }
    str[j] = '\0';
    return str;
}
%}

Ws              ([ \t\r\n]+)
Letter          [A-Za-z]
Digit           [0-9]
NameChar        {Letter}|{Digit}|[-._:]
VersionNum      {Digit}+"."{Digit}+
/* VersionInfo     {Ws}"version"{Ws}?"="{Ws}?("'"{VersionNum}"'"|\"{VersionNum}\") */
VersionInfo     {Ws}"version"{Ws}?"="{Ws}?{VersionNum}
EncName         {Letter}({Letter}|{Digit}|[._]|"-")*
/* EncodingDecl    {Ws}"encoding"{Ws}?"="{Ws}?(\"{EncName}\"|"'"{EncName}"'") */
EncodingDecl    {Ws}"encoding"{Ws}?"="{Ws}?{EncName}
 /* XMLDecl         "<?"[xX][mM][lL]{VersionInfo}{EncodingDecl}?{Ws}?"?>" */
 /* EncodingDecl    {Ws}"encoding"{Ws}?"="{Ws}?{EncName} */
XMLDecl         [xX][mM][lL]
Attributes      ({VersionInfo})?({EncodingDecl})
EmptyElement	"<"{Ws}?{NameChar}+{Ws}?"/"{Ws}?">"

%s XMLBEGIN
%s XMLEND
%s ELEMENTBEGIN

%%
<INITIAL>"<?"{XMLDecl}   {printf("xml declaration"); BEGIN XMLBEGIN;}
<XMLEND>"<?"{XMLDecl}   {printf("xml declaration"); BEGIN XMLBEGIN;}
<XMLBEGIN>"?>"          {BEGIN XMLEND;}
<XMLBEGIN>{Attributes}  {printf("%s\n", yytext);}
<XMLEND>{EmptyElement}	{
							char* string = stringRemoveNonAlphaNum(yytext); 
						 	printf("empty element name=%s\n", string);
						 	BEGIN ELEMENTBEGIN;
						}
%%

int yywrap(){ return 1;}
int main(){ yylex(); }